# Palabrertijos

Game for learning words in other languages.

## Download

* [APK (signed)](https://gitlab.com/NikaZhenya/palabrertijos/raw/master/apk/app-release.apk)
* [APK (not signed)](https://gitlab.com/NikaZhenya/palabrertijos/raw/master/apk/app-debug.apk)

## Included dictionaries

* Spanish - French
* Spanish - French (Numbers)

Feel free to [contact me](mailto:hi@perrotuerto.blog) if you want to add more dictionaries.

## License

Software under [GPLv3+](https://gnu.org/licenses/gpl.html).
