var language = {
  en: {
    new: 'New',
    continue: 'Continue',
    scores: 'Scores',
    back: 'Back',
    end: 'End',
    start: 'Start',
    score: 'Score',
    about: 'About',
    challenges: 'Number of challenges',
    balance: 'Guessing balance',
    new_record: 'New record!',
    dics: [
      ['Spanish - French', 'es-fr'],
      ['Spanish - French (Numbers)', 'es-fr_numbers']
    ],
    collab: [
      ['Developer', 'Nika Zhenya', 'https://perrotuerto.blog'],
      ['French content', 'Idiomas Astalaweb', 'http://idiomas.astalaweb.com/franc%C3%A9s/franc%C3%A9s.asp'],
      ['', 'Números en Francés', 'https://numerosfrances.com/'],
      ['', 'Aprende Francés', 'https://www.aprendefrances.com/listas/numeros/siete/']
    ]
  },
  es: {
    new: 'Nuevo',
    continue: 'Continuar',
    scores: 'Puntajes',
    back: 'Regresar',
    end: 'Finalizar',
    start: 'Iniciar',
    score: 'Puntaje',
    about: 'Acerca',
    challenges: 'Cantidad de retos',
    balance: 'Balance de adivinación',
    new_record: '¡Nuevo récord!',
    dics: [
      ['Español - Francés', 'es-fr'],
      ['Español - Francés (Números)', 'es-fr_numbers']
    ],
    collab: [
      ['Desarrollador', 'Nika Zhenya', 'https://perrotuerto.blog'],
      ['Contenido en francés', 'Idiomas Astalaweb', 'http://idiomas.astalaweb.com/franc%C3%A9s/franc%C3%A9s.asp'],
      ['', 'Números en Francés', 'https://numerosfrances.com/'],
      ['', 'Aprende Francés', 'https://www.aprendefrances.com/listas/numeros/siete/']
    ]
  }
};

